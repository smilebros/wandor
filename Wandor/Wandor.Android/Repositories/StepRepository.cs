﻿using System;
using System.Collections.Generic;
using Android.App;
using Android.Content;
using Android.Database.Sqlite;
using Wandor.Models;

namespace Wandor.Droid.Repositories
{
    public class StepRepository
    {
        private const string DatabaseName = "step.db3";
        private const string TableName = "StepsTable";
        private const string ColumnId = "_id";
        private const string ColumnCount = "count";
        private const string ColumnDate = "date";
        private const string DateFormat = "d";

        private readonly SQLiteDatabase _database;

        public StepRepository()
        {
            string databasePath = Application.Context.GetDatabasePath(DatabaseName).AbsolutePath;
            _database = SQLiteDatabase.OpenOrCreateDatabase(databasePath, null);
            EnsureTableExists();
        }

        private void EnsureTableExists()
        {
            string sql = "SELECT name FROM sqlite_master WHERE type=? AND name=?";
            var tableCursor = _database.RawQuery(sql, new[] { "table", TableName });
            if (!tableCursor.MoveToFirst())
            {
                CreateTable();
            }
        }

        private void CreateTable()
        {
            string sql = $"CREATE TABLE {TableName}({ColumnId} INTEGER PRIMARY KEY AUTOINCREMENT, {ColumnCount} INTEGER, {ColumnDate} TEXT)";
            _database.ExecSQL(sql);
        }

        public void InsertOrUpdateSteps(Step step)
        {
            if (GetStepByDate(step.Date) != null)
            {
                Update(step);
            }
            else
            {
                Insert(step);
            }
        }

        private void Update(Step step)
        {
            using var contentValues = new ContentValues();
            contentValues.Put(ColumnCount, step.Count);
            contentValues.Put(ColumnDate, step.Date.ToString(DateFormat));

            _database.Update(TableName, contentValues, $"{ColumnDate}=?", new[] { step.Date.ToString(DateFormat) });
        }

        private void Insert(Step step)
        {
            using var contentValues = new ContentValues();
            contentValues.Put(ColumnCount, step.Count);
            contentValues.Put(ColumnDate, step.Date.ToString(DateFormat));

            _database.Insert(TableName, null, contentValues);
        }

        public Step GetStepByDate(DateTime date)
        {
            var queryColumns = new List<string> { ColumnCount, ColumnDate };
            var cursor = _database.Query(TableName, queryColumns.ToArray(), $"{ColumnDate}=?", new[] { date.ToString(DateFormat) }, null, null, null);

            if (cursor.MoveToFirst())
            {
                return new Step
                {
                    Count = cursor.GetInt(queryColumns.IndexOf(ColumnCount)),
                    Date = DateTime.Parse(cursor.GetString(queryColumns.IndexOf(ColumnDate))).Date
                };
            }
            return null;
        }
    }
}
