﻿using System;
using Wandor.Models;
using Wandor.Services;

namespace Wandor.Droid.Crosses
{
    public class StepServiceProxy : IStepService
    {
        private IStepService _stepService;

        public Step CurrentStep => _stepService?.CurrentStep;

        public event EventHandler<Step> CurrentStepChanged;

        public void Wrap(IStepService stepService)
        {
            UnWrap();
            _stepService = stepService;
            _stepService.CurrentStepChanged += OnStepCountChanged;
            OnStepCountChanged(this, CurrentStep);
        }

        private void OnStepCountChanged(object sender, Step e)
        {
            CurrentStepChanged?.Invoke(sender, e);
        }

        public void UnWrap()
        {
            if (_stepService != null)
            {
                _stepService.CurrentStepChanged -= OnStepCountChanged;
            }
        }
    }
}
