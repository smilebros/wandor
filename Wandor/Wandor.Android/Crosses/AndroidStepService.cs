﻿using System;
using System.Timers;
using Android.Hardware;
using Android.Runtime;
using Wandor.Droid.Repositories;
using Wandor.Models;
using Wandor.Services;

namespace Wandor.Droid.Crosses
{
    public class AndroidStepService : Java.Lang.Object, ISensorEventListener, IStepService
    {
        private readonly Timer _saveTimer;
        private readonly StepRepository _stepRepository;

        public AndroidStepService()
        {
            _saveTimer = new Timer(3_000)
            {
                AutoReset = false
            };
            _saveTimer.Elapsed += (o, e) => Save(true);
            _stepRepository = new StepRepository();

            Load();
        }

        public Step CurrentStep { get; private set; }

        public event EventHandler<Step> CurrentStepChanged;

        public void OnAccuracyChanged(Sensor sensor, [GeneratedEnum] SensorStatus accuracy)
        {
        }

        public void OnSensorChanged(SensorEvent e)
        {
            int currentTotalStepCount = (int)e.Values[0];
            OnStepCountChanged(currentTotalStepCount);
        }


        private void Load()
        {
            var today = DateTime.Today;
            CurrentStep = _stepRepository.GetStepByDate(today) ?? new Step { Date = today };
        }

        private void Save(bool immediately = false)
        {
            if (CurrentStep == null)
            {
                return;
            }
            if (immediately)
            {
                _stepRepository.InsertOrUpdateSteps(CurrentStep);
            }
            else
            {
                _saveTimer.Stop();
                _saveTimer.Start();
            }
        }

        private void OnStepCountChanged(int value)
        {
            UpdateStep(value);

            InvokeStepCountChanged();
        }

        private void UpdateStep(int value)
        {
            var today = DateTime.Today;
            if (today != CurrentStep.Date)
            {
                // If date changed, save old record; reset _offset value; create new Step record
                Save();
                NewStep(today, value);
            }
            else
            {
                // else save record almost every 10 steps
                CurrentStep.Count += value;
                Save();
            }
        }

        private void NewStep(DateTime today, int value)
        {
            CurrentStep = new Step
            {
                Date = today,
                Count = value
            };
            Save();
        }

        private void InvokeStepCountChanged()
        {
            CurrentStepChanged?.Invoke(this, CurrentStep);
        }
    }
}
