﻿using Android.App;
using Android.Content;
using Android.Hardware;
using Android.OS;
using Android.Runtime;
using Wandor.Droid.Crosses;

namespace Wandor.Droid
{
    [Service]
    public class StepSensorService : Service
    {
        public const int ServiceRunningNotificationId = 10000;
        private const string ApplicationNotificationChannelId = "com.miehaha.wandor.NotificationChannel";

        private AndroidStepService _stepService;
        private StepSensorServiceBinder _binder;


        #region Lifecycle

        public override IBinder OnBind(Intent intent)
        {
            _binder = new StepSensorServiceBinder()
            {
                StepService = _stepService
            };

            return _binder;
        }

        public override bool OnUnbind(Intent intent)
        {
            return true;
        }

        public override void OnRebind(Intent intent)
        {
            base.OnRebind(intent);
        }

        public override void OnCreate()
        {
            base.OnCreate();

            _stepService = new AndroidStepService();

            RegisterNotificationChannel();
            RegisterSensorEventListener();

            if (Build.VERSION.SdkInt >= BuildVersionCodes.O)
            {
                StartAsForeground();
            }
        }

        [return: GeneratedEnum]
        public override StartCommandResult OnStartCommand(Intent intent, [GeneratedEnum] StartCommandFlags flags, int startId)
        {
            return base.OnStartCommand(intent, flags, startId);
        }

        public override void OnDestroy()
        {
            UnregisterSensorEventListener();
            base.OnDestroy();
        }

        #endregion


        private void StartAsForeground()
        {
            // Code not directly related to publishing the notification has been omitted for clarity.
            // Normally, this method would hold the code to be run when the service is started.
            using var notification = new Notification.Builder(this, ApplicationNotificationChannelId)
                .SetContentTitle(Resources.GetString(Resource.String.step_sensor_service_title))
                .SetContentText(Resources.GetString(Resource.String.step_sensor_service_content))
                .SetSmallIcon(Resource.Drawable.xamarin_logo)
                .SetOngoing(true)
                .Build();
            //Enlist this instance of the service as a foreground service
            StartForeground(ServiceRunningNotificationId, notification);
        }

        private void RegisterNotificationChannel()
        {
            var manager = (NotificationManager)GetSystemService(NotificationService);
            var channel = manager.GetNotificationChannel(ApplicationNotificationChannelId);
            if (channel == null)
            {
                channel = new NotificationChannel(ApplicationNotificationChannelId,
                                                  Resources.GetString(Resource.String.step_sensor_service_notification_channel_name),
                                                  NotificationImportance.Min);
                channel.EnableLights(false);
                channel.EnableVibration(false);
                manager.CreateNotificationChannel(channel);
            }
        }

        private void RegisterSensorEventListener()
        {
            using var sensorManager = (SensorManager)GetSystemService(SensorService);
            using var stepSensor = sensorManager.GetDefaultSensor(SensorType.StepDetector);
            sensorManager.RegisterListener(_stepService, stepSensor, SensorDelay.Normal);
        }

        private void UnregisterSensorEventListener()
        {
            using var sensorManager = (SensorManager)GetSystemService(SensorService);
            sensorManager.UnregisterListener(_stepService);
        }
    }
}
