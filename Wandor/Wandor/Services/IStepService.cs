﻿using System;
using Wandor.Models;

namespace Wandor.Services
{
    public interface IStepService
    {
        Step CurrentStep { get; }
        event EventHandler<Step> CurrentStepChanged;
    }
}
