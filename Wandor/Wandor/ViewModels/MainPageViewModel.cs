﻿using System;
using Prism.Navigation;
using Wandor.Models;
using Wandor.Services;

namespace Wandor.ViewModels
{
    public class MainPageViewModel : ViewModelBase
    {
        private readonly IStepService _stepService;

        private int _currentStepCount;
        private DateTime _currentDate;
        private int _currentEnergy;

        public MainPageViewModel(INavigationService navigationService, IStepService stepService) : base(navigationService)
        {
            Title = "Wonder";
            _stepService = stepService;
        }

        public int CurrentStepCount { get => _currentStepCount; set => SetProperty(ref _currentStepCount, value); }

        public DateTime CurrentDate { get => _currentDate; set => SetProperty(ref _currentDate, value); }
        public int CurrentEnergy { get => _currentEnergy; private set => SetProperty(ref _currentEnergy, value); }

        public override void OnNavigatedTo(INavigationParameters parameters)
        {
            base.OnNavigatedTo(parameters);

            BindSource();
        }

        private void BindSource()
        {
            _stepService.CurrentStepChanged += StepCountChanged;
        }

        public override void OnNavigatedFrom(INavigationParameters parameters)
        {
            base.OnNavigatedFrom(parameters);

            UnbindSource();
        }

        private void UnbindSource()
        {
            _stepService.CurrentStepChanged -= StepCountChanged;
        }

        private void StepCountChanged(object sender, Step e)
        {
            CurrentStepCount = e.Count;
            CurrentDate = e.Date;
        }

        public override void Destroy()
        {
            UnbindSource();

            base.Destroy();
        }
    }
}
