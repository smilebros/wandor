﻿using System;

namespace Wandor.Models
{
    public class Step
    {
        public DateTime Date { get; set; }
        public int Count { get; set; }
    }
}
